This project is part of the Digital Fabrication class in 2023, led by Romain di Vozzo at University Paris-Saclay.
The author of the project is me, Katerina Koleva, an M1 master student in Human-Computer Interaction and Design at EIT Digital Master School.

The constraints of this project can be found here: http://romaindivozzo.gitlab.io/teaching/eit/.

In a few words, we need to design and use a 3d printer to print something that covers a surface or a human or an animal that is made out of small pieces that connect to one another without using other materials like glue. The pieces should be 1mm thin and cover a total surface of around 1 sq.m.

My personal idea started with designing a belt, going to a bow tie, a dress and finally, It will be a top. Here I will be updating my whole design process week by week until I reach the end of the class and show the final result.

The small pieces making up the wearable are inspired by the Bulgarian shevitsas - a beautiful embroidery my ancestors used to put on clothes. In them are encoded various symbols that have been part of the lifestyle and culture of the Bulgarians. They can be seen as a type of art that conveys a wish that is then attached on clothing - usually about health, happiness, love or prosperity.

This project is very personal for me, because I also have a tattoo of a shevitsa on my arm and I am originally from Bulgaria. This wearable piece made of small flat 3d printed shevitsas is a way for me to express myself and turn an old tradition into a modern fashion piece that carries love and hope of what it could be, not what it once was.


![ ALT](/img/1.png)