The second week started with a few of us changing the 3D printer filament with the biodegradable ColorFabb allPHA that we are going to use for the project. For me, it was an exciting moment, because I had not worked with a 3d printer on my own before. Below is the picture of the class and the filament.

![ ALT](/img/colorFabb.jpg)
![ ALT](/img/fablab1.jpeg)

I worked more on Fusion 360 to do better the design I planned in my first week and after I cleared some questions about how to make the piece more symmetrical and be able to extrude it.

![ ALT](/img/better1stpiece.png)

After that, the teacher helped me set up the Pruca Slicer settings on my laptop to be able to print my first piece.
Some of the settings here bellow.

![ ALT](/img/pruca_settings1.png)
![ ALT](/img/pruca_settings2.png)

My first print was 1mm thick and on 180 temperature and as a 0.3 DRAFT. Although it was recommended 190-200, another student found out it was not working so well and advised me to try with less.

![ ALT](/img/filament-settings.png)
![ ALT](/img/pruca-slicer-30-draft.png)

I did not manage to finish, because I had to stop it near the end, the pieces were not staying in place. 
Nevertheless, they looked okay and the interactions between the pieces worked.

![ ALT](/img/first3Dprint.jpg)
![ ALT](/img/2piecesconnect.jpg)

I measured the piece and it was a little over 0.9 mm so I decided to make it thinner the next time.

![ ALT](/img/measureThick.jpg)

On my second try, I changed a bit the settings - did 0.8 thickness, did 3 pieces and 0.2 QUALITY print. Didn't have to stop the print but it was close, they were a bit folded.

![ ALT](/img/second-try.png)
![ ALT](/img/second3Dprint.jpg)

Then I played around with the 5 pieces I had, how I can connect them and the possible shapes I can come up with.
The second try was visibly better than the first and the pieces were more smooth.

![ ALT](/img/fivePieces.jpg)
![ ALT](/img/folded.jpg)
![ ALT](/img/sculpture.jpg)

I then got interested in the other fashion design pieces out there that have this same idea of a modular 2d pattern that connected makes a dress or another fashion item. I also found generative design pieces online and many others and started rethinking my design and what I want to cover. I think if I want to make clothing I should smooth the edges of my pieces and probably use fabric between the body and the 3d printed clothing...but more on that in week 3.