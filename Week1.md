My idea for the project was to be something wearable and useful. First thought, complaing with the constraints was to design a belt from small pieces that connect to one another and can be resized for everybody.

![ ALT](/img/sketches.jpg)

Then my second idea, which I find more original and would like to explore is to create a bow tie. Here I have multiple possibilities, it can have pieces connecting together around the neck or maybe a bit more comfortable would be if the bow tie covers a button. To make it more interesting, and because the pieces need to be only 1mm thick, there could be a multiple 2d pieces connected togheter to achieve 3d form of the bow tie.

In this week I also tried using Fusion 360 to design a first test piece that I would like to 3d print. There were a few issues and questions I had about sizing the piece right and making it symetrical and possible to fit with another piece. After clearing those questions, I would like to 3d print it and that is how I want to start week 2.

![ ALT](/img/first_piece.png)

A few links I saved for inspiration, including the design of the piece I want to 3d print first, can be found bellow.

https://www.thingiverse.com/thing:4512781
https://cults3d.com/en/3d-model/fashion/light-bulb-bow-tie
https://monocircus.com/en/bow-tie.html
https://medium.com/@CarolinVogler/modular-fashion-c98306c820a9

Bulgarian embroidery symbols:

![ ALT](/img/sh034%D0%B0.png)