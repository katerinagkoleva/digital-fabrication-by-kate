In week 3 there was a change in the direction of the project from the initial idea of a belt to go to a dress or a bra made out of small weird geometrical forms inspired by the Bulgarian [shevitsa.](https://preslavaivanova.be/explore/discover-shevitsa-history-symbolism-and-meaning/) 
The traditional Bulgarian embroidery is a wonderful art that goes back to ancient times, and it was a way to show information about the person who wore the designs, fundamentally their marital status, their social class and their place of origin. According to the ancient beliefs, evil (diseases, curses, “bad eyes”, all evil forces) strikes man just where the body is unprotected by clothes, and the clothes were conceived as a second skin and the embroidery has the function of protecting the person. The embroidery must be placed on the edges of the woman’s head, the bottom of her shirt, the sleeves and the socks of the male and female garments.

This is an example of shevitsa embroidery.
![ ALT](/img/traditional-bulgarian.jpg)

Since I have a tattoo of one modern version of 'elbetica' (which is a type of shevitsa), I first thought to recreate that piece but soon discovered it would be too many details to add. The symbol represents the four directions in the world (north, south, east, west) merging in a common center. This is also how the cycle of nature is interpreted - winter, summer, spring, autumn.
![ ALT](/img/tattoo.jpg)

For me, expressing myself through 3D printed pieces of шевици is a way to keep my roots but modernize tradition and show hope for what could be, not what it once was.

I did a simple version of the shevitsa and printed it just to see how it looks
![ ALT](/img/edgy_1st_try.jpeg)

Then I spend a lot of time trying to go for more smooth, not so edgy design of the same thing.

![ ALT](/img/in_the_process_of_smoothing.png)
![ ALT](/img/cut-that-form%20from%20the%20other_using_trimming_tools.png)
![ ALT](/img/real_size_smooth_sliced_0.6thick.png)
![ ALT](/img/too_small.jpg)

Then I printed one too-small version and then one too-big.
The connections worked and even there were 2 ways of connecting - straight with 1 or diagonal with 2.
![ ALT](/img/connecting_2.jpg)
![ ALT](/img/connected_diagonal.jpg)

The problem was that the connector was too thin and I changed that in my design.