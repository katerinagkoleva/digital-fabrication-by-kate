In week 4 I already decided on a wearable dress or bra that has a fabric underneath and the 3d-printed layer comes on top.
The problem from last week was that the connectors were too thin at one part and could easily break. This was taken into account and I changed the design of the piece.
![ ALT](/img/changing_thin_part.png)

I decided to go for a 0.5 mm thickness of the piece, 80 mm long and wide.
Since I discovered that the Prusa slicer settings matter I again changed from 0.3 DRAFT hoping there would be fewer holes with 0.15 quality. 

![ ALT](/img/prusa_sett-1.png)
The results were better, but the time it took increased.
I printed 4 black and at the same time as 4 white pieces, hopping to do a mixed design for the wearable.
![ ALT](/img/4shev.png)
![ ALT](/img/two_prints.jpg)

I thought that the black ones are looking better and apparently there are some differences between the colours of the filament ColorFabb, but after another experiment, I realized I was wrong, it was somehow one of the printer's faults. 
Although with the same settings, starting the same print on 2 different printers had a different result, as one of them was ruined.
![ ALT](/img/ruined.jpg)
![ ALT](/img/black_white.jpg)
I did print a lot of pieces, some had small holes in them, some were ruined and some turned out okay. I have a favourite printer now, and I like a bit more the texture of the black pieces.
![ ALT](/img/testing.jpg)

Finally, I started playing around with the shevitsa pieces, trying to find the best ways to connect them.
![ ALT](/img/20230207_201119.jpg)

My calculations show that I need 169 pieces, I already have 9 good ones from all the testing, so 160 more to go.
I am only sure about one of the printers and to print 4 pieces on one plate takes 59min on the 0.15 quality setting that seems to work best.
![ ALT](/img/calcul.png)

I need another 40h in the lab to achieve that, if I use the one printer that does not make holes and does not ruin pieces. I would maybe prefer to stop once I have enough for a dress, because 1 sq.m can be a bit too much.




