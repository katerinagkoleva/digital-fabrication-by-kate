Week 7 was basically the day of the presentation-21.02.2023.
I started that day by puting the piece on so a friend can make a demo video of it.

![ ALT](/img/puting-it-on.jpeg)

It was incredibly hard connecting the pieces on my back and I managed to again break 3 pieces while doing that. I wanted it to be a bit more thighter but I couldn't connect so I let them be a little loose around my breasts.
I then wanted to put something over it before going out. It is a bit itchy around the arms but still fine.

Here is me going to uni that day to present it:

![ ALT](/img/RerB-shot.jpg)

And demo pictures and videos down bellow:

![ ALT](/img/Hero-shot.JPG)
![ ALT](/img/sun_shot.JPG)
![ ALT](/img/back_shot.JPG)
![overview](0-video.mp4)
![closeup strech](1_video.mp4)
![closeup turn](2_video.mp4)

Before the presentation, it disconnected one time and I had to exchange a piece, luckily I did have extras with me.
That day I wore it from 9 to 17 and took it to lunch, the train, and the bus, I was a photographer for a friend's project and managed to wear it while presenting my documentation to the class.

Overall, I am really happy with the result and the class. I am inspired to do more projects like this one in the future! Also, I liked that the filament is biodegradable and what I made is also a sustainable luxury fashion item!