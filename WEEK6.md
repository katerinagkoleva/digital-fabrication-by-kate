In week 6 I started drawing designs of the whole wearable. I now have connected 3 by 3 squares of pieces and one belt of 14 pieces and started thinking of ways to cover my body, because it is not going to be linear like covering a flat surface.
Trying to figure out the connections in canva and some sketches:
![ ALT](/img/2.png)

![ ALT](/img/sketch.jpg)
After some thought, I made another 3 by 3 square and decided to connect the two squares from one edge (on purpose one started with shevitsa having pointy rotation and the other one with hook rotation).
Here are the two squares connected to the belt.

![ ALT](/img/proces_assemble.jpg)

Then I added the straps and a second connection point on the back coming from the other edges of those base squares.

![ ALT](/img/ready_to_wear.jpg)

Those are the photos of me after I successfully managed to assemble it from behind, it connects in 2 places: one from the straps and one from the belt. In total, I used 67 pieces for this design, but I needed to have more, because some of them broke during the assembling process.

![ ALT](/img/fav_photo.jpg)

Here is very visible the similarity between my shevitsa tattoo and the pieces of the wearable.

![ ALT](/img/tattoo_and_wearable.jpg)

One of the small things I didn't see was that on the back there were 2 pieces missing, so after I removed the wearable from myself I added them.

![ ALT](/img/back_2miss.jpg)

Overall, I was really happy that there was a result of my tries and that is my smile when I wore that in the main building of Cite Universitaire. Some improvements, of course, should be made!

![ ALT](/img/big_smile_after_I_could_wear_that.jpg)

After, I added some pieces where there were visible holes and left the piece hanging on my chair. The whole wearable should have around 73 pieces now.


![ ALT](/img/chair.jpg)