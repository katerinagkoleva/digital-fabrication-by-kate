# Summary

* [Introduction](README.md)

* [WEEK 1](Week1.md)

* [WEEK 2](WEEK2.md)

* [WEEK 3](WEEK3.md)

* [WEEK 4](WEEK4.md)

* [WEEK 5](WEEK5.md)

* [WEEK 6](WEEK6.md)

* [Presentation Day](PresentationDay.md)

![ ALT](/img/Hero-shot.JPG)