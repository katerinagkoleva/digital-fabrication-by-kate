In week 5 I already started thinking about the design of the dress. I decided to buy undergarmet, since the pieces have holes in them.

I started going to the fablab almost every day to print pieces. One of the printers went out of order, because a filament got stuck in. Now the whole class has only 2 printers to finish their projects and the queues get long...

However, this is the start of the dress (or possibly a top if I don't have enough pieces).

![ ALT](/img/belt-front.jpg)
![ ALT](/img/belt-back.jpg)

Some of my pieces still get ruined from the printer and this is a matter of chance... the best results come when the setting is set to quality and the layers are thinner. For pieces to not get melted together best is to print them one by one, but this is not always possible.

I also have a problem sometimes disconnecting pieces, since they can break. Because of this, I would need extra pieces to be sure that the whole design is achieved.

Inspiration for the upper part is taken from [this blog post](https://medium.com/@CarolinVogler/modular-fashion-c98306c820a9).

![ ALT](/img/inspiration.png)
